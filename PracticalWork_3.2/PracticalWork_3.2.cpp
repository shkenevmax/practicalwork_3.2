﻿// PracticalWork_3.2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <vector>

using namespace std;

class BeginIter
{
public:
	BeginIter(vector<int> *NewConteiner);

	int pos = 0;

	BeginIter& operator++();
	BeginIter& operator--();

private:
	vector<int> MyVector;
	
};

BeginIter::BeginIter(vector<int> *NewConteiner) : MyVector(*NewConteiner) {}

BeginIter& BeginIter::operator++()
{
	if (pos >= 0 && pos < MyVector.size()) cout << MyVector.at(pos++) << endl;
	return *this;
}

BeginIter& BeginIter::operator--()
{
	if (pos >= 0 && pos < MyVector.size()) cout << MyVector.at(pos--) << endl;
	return *this;
}

int main()
{
	vector<int> nums{ 4, 8, 9, 7, 3 };
	BeginIter BegIterator(&nums);

	int i = nums.size();
	while (i > 0)
	{
		++BegIterator;
		i--;
	}
}